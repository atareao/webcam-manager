/*
 * Clipman
 * A manager for the Clipboard
 *
 * Copyright (C) 2018
 *     Lorenzo Carbonell <lorenzo.carbonell.cerezo@gmail.com>,
 *
 * This file is part of Clipman.
 * 
 * Clipman is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clipman is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gnome-shell-extension-openweather.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */


/*
imports.gi.versions.Gio = "2.0";
imports.gi.versions.GLib = "2.0";
imports.gi.versions.GObject = "2.0";
imports.gi.versions.Gtk = "3.0";
imports.gi.versions.Meta = "1";
imports.gi.versions.Pango = "1.0";
imports.gi.versions.Shell = "0.1";
*/
imports.gi.versions.St = "1.0";
imports.gi.versions.Clutter = "1.0";
imports.gi.versions.Gtk = "3.0";
imports.gi.versions.Gio = "2.0";
imports.gi.versions.GLib = "2.0";

/* Import St because is the library that allow you to create UI elements */
const St = imports.gi.St;
/* Import Clutter because is the library that allow you to layout UI elements */
const Clutter = imports.gi.Clutter;

const Gtk = imports.gi.Gtk;
const Gdk = imports.gi.Gdk;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Params = imports.misc.params;

const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;

const ExtensionUtils = imports.misc.extensionUtils;
const Extension = ExtensionUtils.getCurrentExtension();
const Convenience = Extension.imports.convenience;
const Gettext = imports.gettext.domain(Extension.uuid);
const _ = Gettext.gettext;


class WebcamManager extends PanelMenu.Button{

    constructor(){
        super(St.Align.START);

        Gtk.IconTheme.get_default().append_search_path(
            Extension.dir.get_child('icons').get_path());

        let box = new St.BoxLayout();
        let label = new St.Label({text: 'Button',
                                   y_expand: true,
                                   y_align: Clutter.ActorAlign.CENTER });
        //box.add(label);
        this.icon = new St.Icon({icon_name: 'webcam-enabled',
                                 style_class: 'system-status-icon'});
        box.add(this.icon);
        //box.add(PopupMenu.arrowIcon(St.Side.BOTTOM));
        this.actor.add_child(box);

        this.webcamSwitch = new PopupMenu.PopupSwitchMenuItem(_('Disable Webcam'),
                                                                {active: true})

        this.menu.addMenuItem(this.webcamSwitch)

        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        this.settingsMenuItem = new PopupMenu.PopupMenuItem(_("Settings"));
        this.settingsMenuItem.connect('activate', () => {
            GLib.spawn_command_line_async(
                "gnome-shell-extension-prefs webcam-manager@atareao.es"
            );
        });

        this.menu.addMenuItem(this.settingsMenuItem);
        this.menu.addMenuItem(this._get_help());

        this.webcamEnabled = true;

        this.webcamSwitch.connect('toggled', (widget, value) => {
            if(value)
            {
                // If webcam is off then turn on
                if(!this._webcam_on()){
                   this.set_webcam_on(true);
                }
            }
            else
            {
                // If webcam is on then turn off
                if(this._webcam_on()){
                   this.set_webcam_on(false);
                }
            }
        });
        this.sourceId = 0;
        this._init_webcamSwitch();
        this.settings = Convenience.getSettings();

        this.settings.connect('changed', ()=>{
            if(this.sourceId > 0){
                GLib.source_remove(this.sourceId);
                this.sourceId = 0;
            }
            let monitor = this.settings.get_value('monitor').deep_unpack();
            let watch_time = this.settings.get_value('watch-time').deep_unpack();
            if(monitor == true && watch_time > 0){
                this.sourceId = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT,
                    watch_time,
                    () => {
                        this._init_webcamSwitch();
                        return true;
                    });
            }
        });
    }

    set_webcam_on(toon){
        let success, pid, args;
        if(toon){
            args = ['sudo', 'modprobe', '-av', 'uvcvideo'];
        }else{
            args = ['sudo', 'modprobe', '-rv', 'uvcvideo'];
        }

        try {
            [success, pid] = GLib.spawn_async(null, args, null,
                                              GLib.SpawnFlags.SEARCH_PATH | GLib.SpawnFlags.DO_NOT_REAP_CHILD,
                                              null);
        } catch (error) {
            log(error);
        }
        // adds a watch to know when the child process ends
        GLib.child_watch_add(GLib.PRIORITY_DEFAULT, pid, (pid, status)=>{
            GLib.spawn_close_pid(pid);
            this._init_webcamSwitch();
        }, null);
    }

    _init_webcamSwitch(){
        if(this._webcam_on()){ // Kernel module loaded
            this.webcamSwitch.label.set_text(_('Disable Webcam'));
            this.icon.set_icon_name('webcam-enabled');
            this.webcamSwitch.setToggleState(true);
        }else{ // Kernel module not loaded
            this.webcamSwitch.label.set_text(_('Enable Webcam'));
            this.icon.set_icon_name('webcam-disabled');
            this.webcamSwitch.setToggleState(false);
        }
    }

    _webcam_on(){
        let lsmod_string = GLib.spawn_command_line_sync('lsmod')[1].toString();
        let regex = /uvcvideo/gm;
        return !(regex.exec(lsmod_string) == null);
    }

    _create_help_menu_item(text, icon_name, url){
        let menu_item = new PopupMenu.PopupImageMenuItem(text, icon_name);
        menu_item.connect('activate', () => {
            Gio.app_info_launch_default_for_uri(url, null);
        });
        return menu_item;
    }

    _get_help(){
        let menu_help = new PopupMenu.PopupSubMenuMenuItem(_('Help'));
        menu_help.menu.addMenuItem(this._create_help_menu_item(
            _('Project Page'), 'gitlab', 'https://gitlab.gnome.org/atareao/webcam-manager'));
        menu_help.menu.addMenuItem(this._create_help_menu_item(
            _('Get help online...'), 'help-online', 'https://www.atareao.es/aplicacion/webcam-manager/'));
        menu_help.menu.addMenuItem(this._create_help_menu_item(
            _('Translate this application...'), 'translate', 'https://translations.launchpad.net/webcam-manager'));
        menu_help.menu.addMenuItem(this._create_help_menu_item(
            _('Report a bug...'), 'bug', 'https://gitlab.gnome.org/atareao/webcam-manager/issues'));
        menu_help.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
        menu_help.menu.addMenuItem(this._create_help_menu_item(
            _('El atareao'), 'web', 'https://www.atareao.es'));
        menu_help.menu.addMenuItem(this._create_help_menu_item(
            _('Follow me in Twitter'), 'twitter', 'https://twitter.com/atareao'));
        menu_help.menu.addMenuItem(this._create_help_menu_item(
            _('Follow me in Facebook'), 'facebook', 'http://www.facebook.com/elatareao'));
        menu_help.menu.addMenuItem(this._create_help_menu_item(
            _('Follow me in Google+'), 'google', 'https://plus.google.com/118214486317320563625/posts'));
        return menu_help;
    }
}

var button;

function init() {
    Convenience.initTranslations();
}

function enable() {
    button = new WebcamManager();
    Main.panel.addToStatusArea('Webcam Manager', button, 0, 'right');
}

function disable() {
    if(button.sourceId > 0){
        GLib.source_remove(button.sourceId);
    }
    button.destroy();
}
